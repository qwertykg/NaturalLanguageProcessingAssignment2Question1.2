package algorithm;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import org.apache.commons.lang.ArrayUtils;

public class BigramCalculator 
{
	public Map<String, Integer> unigramCounts;
	public Map<String, Integer> bigramCounts;
	
	public Map<String, Double> unigramProbabilities;
	public Map<String, Double> bigramProbabilities;
	
	public String[] tokens;

	int smoothing = 0;

	public void calculateNgrams(String contents) 
	{
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog (null, "Would you like to use La-Placian smoothing?", "Use Smoothing?", dialogButton);
		
		if (dialogResult == JOptionPane.YES_OPTION)
		{
			smoothing = getSmoothing();
		}
		
		tokens = getTokens(contents);
		
		unigramCounts = getCountsForUniGram(tokens);
		
		if(smoothing != 0)
		{
			unigramCounts = addSmoothing(unigramCounts, smoothing);
		}
		
		unigramProbabilities = getProbabilitiesForUniGram(unigramCounts);

		bigramCounts = getCountsForBiGram(tokens);
		
		if(smoothing != 0)
		{
			bigramCounts = addSmoothing(bigramCounts, smoothing);
		}
		
		bigramProbabilities = getProbabilitiesForbigram(bigramCounts);
	}

	private int getSmoothing() 
	{
		return 1;
	}

	private Map<String, Integer> addSmoothing(Map<String, Integer> counts, int smoothing) 
	{
		for(String key : counts.keySet())
		{
			int count = counts.get(key);
			counts.put(key, count + smoothing);
		}
		return counts;
	}

	private String[] getTokens(String contents) 
	{
		contents = getNormalizedContents(contents);
		String [] tokens = contents.split("[., !:;]");
		
		for(String token : tokens)
		{
			if(isEmptyWord(token))
			{
				tokens = (String [])ArrayUtils.removeElement(tokens, token);
			}
		}
		
		return tokens;
	}

	private String getNormalizedContents(String contents) 
	{
		contents = contents.replaceAll("[()'\"]", "");

		return contents;
	}
	
	private boolean isEmptyWord(String token) 
	{
		return token.equals("");
	}
	
	private Map<String, Integer> getCountsForUniGram(String[] tokens) 
	{
		Map<String, Integer> counts = new HashMap<String, Integer>();
		
		for(String token : tokens)
		{
			if(counts.containsKey(token))
			{
				int counter = counts.get(token);
				counter += 1;
				counts.put(token, counter);
			}
			else
			{
					counts.put(token, 1);	
			}
		}
		
		return counts;
	}
	
	private Map<String, Integer> getCountsForBiGram(String[] tokens) 
	{
		Map<String, Integer> counts = new HashMap<String, Integer>();
		String previousWord = "<S>";
		
		for(String token : tokens)
		{
			String bigram = previousWord + " " + token;
			
			if(counts.containsKey(bigram))
			{
				int counter = counts.get(bigram);
				counter += 1;
				counts.put(bigram, counter);
			}
			else
			{
				counts.put(bigram, 1);	
			}
			
			previousWord = token;
		}
		
		Object [] words =  unigramProbabilities.keySet().toArray();
		
		int numberOfDistinctWords = words.length;
		
		Map<String, Integer> tableOfCounts =  new HashMap<String, Integer>();
		
		for (int row = 0; row < numberOfDistinctWords; row++) 
		{
			for (int col = 0; col < numberOfDistinctWords; col++) 
			{
				String bigram = (String)words[row] + " " + (String)words[col];
				if(counts.get(bigram) == null)
				{
					tableOfCounts.put(bigram, 0);
				}
				else
				{
					tableOfCounts.put(bigram, counts.get(bigram));
				}
			}
		}
		
		return tableOfCounts;
	}
	
	private Map<String, Double> getProbabilitiesForUniGram(Map<String, Integer> unigramCounts) 
	{
		Double wordCount = 0.0;
		
		if(smoothing == 0)
		{
			wordCount =  tokens.length * 1.0;
		}
		else
		{
			wordCount = unigramCounts.size() * 1.0 + tokens.length;
		}
		
		Map<String, Double> unigramProbabilities = new HashMap<>();
			
		for(String key : unigramCounts.keySet())
		{
			unigramProbabilities.put(key, unigramCounts.get(key) / wordCount);
		}
		return unigramProbabilities;
	}
	
	private Map<String, Double> getProbabilitiesForbigram(Map<String, Integer> bigramCounts) 
	{
		Map<String, Double> bigramProbabilities = new HashMap<>();
		
		for(String key : bigramCounts.keySet())
		{
			int numerator = bigramCounts.get(key);
			
			Double denominator = 0.0;
			
			if(smoothing == 0)
			{
				denominator =  unigramCounts.get(key.split(" ")[0]) * 1.0;
			}
			else
			{
				denominator = unigramCounts.get(key.split(" ")[0]) + unigramCounts.size() * 1.0;
			}
			

		    bigramProbabilities.put(key, numerator / denominator);
		}

		return bigramProbabilities;
	}

	public void getNextWord(String previousWord) 
	{
		String pattern = "(" + previousWord + ")( )(.+)";
	    
		String currentChoiceForNextWord = null;
		Double probabilityOfCurrentNextWord = 0.0;
		
		for(String key : bigramProbabilities.keySet())
		{
			if(key.matches(pattern))
			{
				Double probability = bigramProbabilities.get(key);
				
				if(probability > probabilityOfCurrentNextWord)
				{
					probabilityOfCurrentNextWord = probability;
					currentChoiceForNextWord = key.split(" ")[1];
				}
			}
		}
		
		JOptionPane.showMessageDialog(null, "The most probable next word is: " + currentChoiceForNextWord + " with a probability of: " + probabilityOfCurrentNextWord);
	}
}
