package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReaderService 
{
	public FileReaderService()
	{}
	
	public String readTextFile(String fileName) throws FileNotFoundException
	{
		File file = new File("trainingData/"+ fileName + ".txt");
	    
		Scanner sc = new Scanner(file);
		String contents = ""; 
	  
		while (sc.hasNextLine())
	    {
	    	contents += sc.nextLine();
	    }
	    
		return contents;
	}
}
