import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

import algorithm.BigramCalculator;
import services.FileReaderService;

public class Main 
{
	FileReaderService fileReader;
	BigramCalculator calculator;
	 
	public Main()
	{
		fileReader = new FileReaderService();
		calculator = new BigramCalculator();
	}
	
	public void startUp() throws FileNotFoundException
	{
		String fileName = JOptionPane.showInputDialog("Enter the training data filename:");
		
		String contents = fileReader.readTextFile(fileName);
		
		calculator.calculateNgrams(contents);
		
		String previousWord = JOptionPane.showInputDialog("Enter the word(Wn-1):");
		
		calculator.getNextWord(previousWord);
	}
	
	public static void main(String[] args) throws FileNotFoundException
	{
		new Main().startUp();
	}
}
